import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
  
@Injectable({
providedIn: 'root',
})
export class AuthInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: { 
                'Access-Control-Allow-Origin': '*',
                // for testing, we just use this access token for acessing to backend api, you can use the other one when registering or logging in with Swagger
                'Authorization': `Bearer eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImY1MTNlZWEzLTBkYzktNDNmNC0wNDhmLTA4ZGEwY2ZlOTNhZiIsImVtYWlsIjoiYWRtaW4xMUBleGFtcGxlLmNvbSIsInJvbGUiOiJhZG1pbiIsImV4cCI6MTk2MzY4MDg3Nn0._gNVdSZUABSfhvXx6sgv4SXsHX4ecKQCFvLLm2cIApM` 
            }
        });

        return next.handle(request);
    }
}
  