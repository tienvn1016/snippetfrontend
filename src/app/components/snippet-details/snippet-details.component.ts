import { Component, OnInit } from '@angular/core';
import { SnippetService } from 'src/app/services/snippet.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Snippet } from 'src/app/models/snippet.model';

@Component({
  selector: 'app-snippet-details',
  templateUrl: './snippet-details.component.html',
  styleUrls: ['./snippet-details.component.css']
})
export class SnippetDetailsComponent implements OnInit {
  currentSnippet: Snippet = {
    title: '',
    textSnippet: ''
  };
  message = '';
  
  constructor(
    private snippetService: SnippetService,
    private route: ActivatedRoute,
    private router: Router) { }
  
  ngOnInit(): void {
    this.message = '';
    this.getSnippet(this.route.snapshot.params.id);
  }

  getSnippet(id: string): void {
    this.snippetService.get(id)
      .subscribe(
        res => {
          this.currentSnippet = res.data;
          console.log(res);
        },
        error => {
          console.log(error);
        });
  }

  updateSnippet(): void {
    this.snippetService.update(this.currentSnippet.id, this.currentSnippet)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message;
        },
        error => {
          console.log(error);
        });
  }
  
  deleteSnippet(): void {
    this.snippetService.delete(this.currentSnippet.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/snippets']);
        },
        error => {
          console.log(error);
        });
  }
}