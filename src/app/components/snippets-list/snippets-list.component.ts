import { Component, OnInit } from '@angular/core';
import { Snippet } from 'src/app/models/snippet.model';
import { SnippetService } from 'src/app/services/snippet.service';

@Component({
  selector: 'app-snippets-list',
  templateUrl: './snippets-list.component.html',
  styleUrls: ['./snippets-list.component.css']
})

export class SnippetsListComponent implements OnInit {
  snippets: Snippet[] = [];
  currentSnippet?: Snippet;
  currentIndex = -1;
  title = '';
  currentSearchTerm = '';
  page = 1;
  count = 0;
  pageSize = 3;
  pageSizes = [3, 6, 9];
  
  constructor(private snippetService: SnippetService) { }

  ngOnInit(): void {
    this.retrieveSnippets();
  }

  retrieveSnippets(): void {
    console.log(`title = ${this.title}, page = ${this.page}, pageSize = ${this.pageSize}`);
    this.snippetService.getSnippets(this.title, this.page, this.pageSize)
      .subscribe(
        res => {
          this.snippets = res.data.items;
          this.count = res.data.totalCount;
          this.currentSnippet = undefined;
          this.currentIndex = -1;
          console.log(res);
          console.log(this.snippets);
        },
        error => {
          console.log(error);
        });
  }

  setActiveSnippet(snippet: Snippet, index: number): void {
    this.currentSnippet = snippet;
    this.currentIndex = index;
  }

  // searchTitle(): void {
  //   this.snippetService.getSnippets(this.title, this.page, this.pageSize)
  //     .subscribe(
  //       res => {
  //         this.snippets = res.data.items;
  //         this.currentSnippet = undefined;
  //         this.currentIndex = -1;
  //         console.log(res);
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }

  searchText(searchTerm: string): void {
    this.currentSearchTerm = searchTerm;
  }

  handlePageChange(event: number): void {
    this.page = event;
    this.retrieveSnippets();
  }
  handlePageSizeChange(event: any): void {
    this.pageSize = event.target.value;
    this.page = 1;
    this.retrieveSnippets();
  }
}