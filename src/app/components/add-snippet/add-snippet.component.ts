import { Component, OnInit } from '@angular/core';
import { Snippet } from 'src/app/models/snippet.model';
import { SnippetService } from 'src/app/services/snippet.service';
@Component({
  selector: 'app-add-snippet',
  templateUrl: './add-snippet.component.html',
  styleUrls: ['./add-snippet.component.css']
})
export class AddSnippetComponent implements OnInit {
  snippet: Snippet = {
    title: '',
    textSnippet: ''
  };
  submitted = false;

  constructor(private snippetService: SnippetService) { }
  
  ngOnInit(): void {
  }
  
  saveSnippet(): void {
    const data = {
      title: this.snippet.title,
      textSnippet: this.snippet.textSnippet
    };

    this.snippetService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newSnippet(): void {
    this.submitted = false;
    this.snippet = {
      title: '',
      textSnippet: ''
    };
  }
}