import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Snippet } from '../models/snippet.model';

// You should change the port here (44316) corresponding to the actual port while running on your local computer
const baseUrl = 'https://localhost:44316/api/v1/snippet'; 


@Injectable({
  providedIn: 'root'
})
export class SnippetService {
  constructor(private http: HttpClient) {
  }

  getSnippets(title: string, page: number, size: number): Observable<any> {
    return this.http.get<Snippet[]>(`${baseUrl}?title=${title}&page=${page}&size=${size}`);
  }

  get(id: any): Observable<any> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }
}