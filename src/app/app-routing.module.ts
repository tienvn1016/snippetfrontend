import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SnippetsListComponent } from './components/snippets-list/snippets-list.component';
import { SnippetDetailsComponent } from './components/snippet-details/snippet-details.component';
import { AddSnippetComponent } from './components/add-snippet/add-snippet.component';
const routes: Routes = [
  { path: '', redirectTo: 'snippets', pathMatch: 'full' },
  { path: 'snippets', component: SnippetsListComponent },
  { path: 'snippets/:id', component: SnippetDetailsComponent },
  { path: 'add', component: AddSnippetComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }